# debian_handbook_en_cours
Ce projet a pour objet d'accueillir les fichiers des différents chapitres de
la traduction du Debian Handbook en cours de révision. Les fichiers
d'origines sont disponibles dans le dépôt de salsa de l'équipe de traduction
en fraçais https://salsa.debian.org/l10n-fr-team/debian-handbook.

J'y déposerai les fichiers modifiés jusqu'au « DONE », étape où je
les reverserai dans le dépôt l10n-fr-team/debian-handbook, ainsi que les
fichiers .diff envoyés à la liste.